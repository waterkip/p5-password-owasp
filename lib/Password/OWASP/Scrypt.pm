package Password::OWASP::Scrypt;
our $VERSION = '0.006';
use Moose;

# ABSTRACT: An Scrypt implemenation of Password::OWASP

with 'Password::OWASP::AbstractBase';

use Authen::Passphrase::Scrypt;
sub ppr { 'Authen::Passphrase::Scrypt' }

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

Implements Scrypt password checking.

=head1 SYNOPSIS

    package MyApp::Authentication;

    use Password::OWASP::Bcrypt;

    my $user = get_from_db();
    my $from_web = "Some very secret password";

    my $owasp = Password::OWASP::Bcrypt->new(
        # optional
        hashing => 'sha512',
        update_method => sub {
            my $password = shift;
            $user->update_password($password);
            return;
        },
    );

    if (!$owasp->check_password($from_web)) {
        die "You cannot login";
    }

=head1 METHODS

=head2 crypt_password

Encrypt the password and return it as an RFC2307 formatted string.

=head2 check_password

Check if the password is the same as what was stored.

=head1 SEE ALSO

=over

=item * L<Password::OWASP::AbstractBase>

=item * L<Authen::Passphrase::BlowfishCrypt>

=back
